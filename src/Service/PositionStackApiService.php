<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class PositionStackApiService{

    private $client;

    public function __construct (HttpClientInterface $client){

        return $this->client = $client;
    }

    public function getCoordinates($address) {

        $response = $this->client->request(
            'GET',
            'http://api.positionstack.com/v1/forward',
            [
                'query' => [
                    'access_key' => '93165b3ed559a82d34e3c115b868b237',
                    'query' => $address,
                    'limit'=> 1
                ],
            ]
          );

        $result = json_decode($response->getContent());

        if(empty($result->data))
          return null;

        return $result->data[0];
    }
}
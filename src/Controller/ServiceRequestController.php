<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\ServiceRequest;
use App\Entity\Mission;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Repository\ServiceRequestRepository;
use App\Service\PositionStackApiService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Domain\CreateServiceRequest;
use App\Entity\RequestHistory;
use App\Entity\MissionHistory;
use App\Repository\MissionRepository;

class ServiceRequestController extends AbstractController{

    /**
     * @Route("/envoyer-une-demande", name="request_posting")
     */
    public function requestPosting(Request $request, EntityManagerInterface $em, PositionStackApiService $positionStackApiService) {

        $user = $this->getUser();

        $createRequest = new CreateServiceRequest();
        $createRequest->setAddress($user->getAddress());
        $createRequest->setZipCode($user->getZipCode());
        $createRequest->setCity($user->getCity()); 

        $form = $this->createFormBuilder($createRequest)
            ->add("title", TextType::class, [
                "attr" => [
                    "class" => "form-control",
                ], 
                "label" => "Titre"
            ])
            ->add("category", EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                "attr" => [
                    "class" => "form-control"
                ], 
                "label" => "Catégorie"
            ])
            ->add("description", TextareaType::class, [
                "attr" => [
                    "class" => "form-control"
                ], 
                "label" => "Description"
            ])
            ->add("servicePlace", TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ], 
                "required" => false,
                "label" => "Endroit où le volontaire devra se rendre (optionnel)"
            ])
            ->add('address', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ],
                "label" => "N° et nom de rue"
            ])
            ->add('zipCode', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ],
                "label" => "Code postal"
            ])
            ->add('city', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ],
                "label" => "Ville"
            ])
            ->add("submit", SubmitType::class,[
                "attr" => [
                    "class" => "btn btn-success"
                ], 
                "label" => "Envoyer ma demande de service"
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $addressQuery = $createRequest->getAddress()." ".$createRequest->getZipCode()." ".$createRequest->getCity();

            $coordinates = $positionStackApiService->getCoordinates($addressQuery);
            if($coordinates == null){
                $this->addFlash('fail', "L'adresse saisie n'est pas reconnue, veuillez vérifier votre saisie svp");
                return $this->render('/sendRequest.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            $serviceRequest = new ServiceRequest();
            $serviceRequest->setTitle($createRequest->getTitle());
            $serviceRequest->setCategory($createRequest->getCategory());
            $serviceRequest->setDescription($createRequest->getDescription());
            $serviceRequest->setServicePlace($createRequest->getServicePlace());
            $serviceRequest->setLatitude($coordinates->latitude);
            $serviceRequest->setLongitude($coordinates->longitude);
            $serviceRequest->setCreatedAt(new \DateTime());
            $serviceRequest->setUpdatedAt(new \DateTime());
            $serviceRequest->setStatus('CREATED');
            $serviceRequest->setBeneficiary($this->getUser());

            $em->persist($serviceRequest);

            $requestHistory = new RequestHistory();
            $requestHistory->setServiceRequest($serviceRequest);
            $requestHistory->setStatus('CREATION');
            $requestHistory->setCreatedAt(new \DateTime());

            $em->persist($requestHistory);

            $em->flush();

            $this->addFlash('success', 'Votre demande de service a bien été créée et est désormais visibles des autres utilisateurs');

            return $this->redirectToRoute('request_posting');
        }else{
            return $this->render('/sendRequest.html.twig', [
                'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/liste-des-demandes", name="services_list")
     */
    public function requestsList( ServiceRequestRepository $requestRepository, Request $request, EntityManagerInterface $em,PositionStackApiService $positionStackApiService) : Response{

        $orderBy = $request->query->get('orderBy');

        if($orderBy == null){
            $orderBy = "updatedAt";
        }

        if($orderBy == "updatedAt"){
            // Trier par dernière date de mise à jour
            $servicesList = $requestRepository
                ->findBy(
                ['status' => 'CREATED'],
                [$orderBy =>  "DESC"]
                );

        }else{
            // Trier par distance la plus proche

            // On récupère les données d'adresse de l'utilisateur        
            $userAddress = $this->getUser()->getAddress();
            $userZipCode = $this->getUser()->getZipCode();
            $userCity = $this->getUser()->getCity();

            // On crée une string qui contient les infos de l'adresse utilisateur pour la passer à l'API    
            $userLocation = $userAddress . ' ' . $userZipCode . ' ' . $userCity;

            // On obtient les coordonnées GPS grâce à l'API
            $userCoordinates = $positionStackApiService->getCoordinates($userLocation);

            $latitude = $userCoordinates->latitude;
            $longitude = $userCoordinates->longitude;

            // Formule de calcul de la distance
            $distanceSQL="(6366*acos(cos(radians($latitude))*cos(radians(s.latitude))*cos(radians(s.longitude) -radians($longitude))
            +sin(radians($latitude))*sin(radians(s.latitude))))";
    
            // On crée une requête en BDD pour obtenir la distance entre la position du user et la position de chaque requête
            $services = $em->createQueryBuilder()
                    ->select('s')
                    ->addSelect($distanceSQL.' AS distance') 
                    ->from('App\Entity\ServiceRequest', 's')
                    ->andWhere('s.status = :requestStatus')
                        ->setParameter('requestStatus', "CREATED")
                    ->add('orderBy', 'distance ASC')
                    ->getQuery()->getResult(); 

            $servicesList = array();
            foreach($services as $service){
                $servicesList[] = $service[0];
            }

        }
        
        return $this->render('/requestsList.html.twig', [
            'servicesList' => $servicesList
        ]);
    }

    /**
     * @Route ("/liste-des-demandes/{id}", name="mission_creation")
     */
     public function missionCreation($id, ServiceRequestRepository $requestRepository, Request $request, EntityManagerInterface $em) : Response {

        $user_id = $this->getUser()->getId();

        $serviceRequest = $requestRepository
        ->findOneBy(
            ['id' => $id]
        );

        $mission = new Mission();

        $form = $this->createFormBuilder($mission)
        ->add("submit", SubmitType::class,[
            "attr" => [
                "class" => "btn btn-success"
            ],
            "label" => "Proposer mon aide à ".$serviceRequest->getBeneficiary()->getPseudo()
        ])
        ->getForm();

         $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
                
            if($user_id == $serviceRequest->getBeneficiary()->getId()){

                $this->addFlash('fail', 'Vous ne pouvez pas vous porter volontaire sur votre propre demande de service');

                return $this->render('/requestDetails.html.twig', 
                        [ 'serviceRequest' => $serviceRequest, 
                        'form' => $form->createView()
                ]);
            }

            $mission->setCreatedAt(new \DateTime());
            $mission->setUpdatedAt(new \DateTime());
            $mission->setServiceRequest($serviceRequest);
            $mission->setStatus('ONGOING');
            $mission->setVolunteer($this->getUser());
            $mission->setBeneficiary($serviceRequest->getBeneficiary());
            $em->persist($mission);

            $associatedMissionHistory = new MissionHistory();
            $associatedMissionHistory->setMission($mission);
            $associatedMissionHistory->setStatus('CREATION');
            $associatedMissionHistory->setCreatedAt(new \DateTime());
            $em->persist($associatedMissionHistory);

            $serviceRequest->setStatus('MISSION');
            $em->persist($serviceRequest);

            $associatedRequestHistory = new RequestHistory();
            $associatedRequestHistory->setServiceRequest($serviceRequest);
            $associatedRequestHistory->setStatus('MISSION');
            $associatedRequestHistory->setCreatedAt(new \DateTime());
            $em->persist($associatedRequestHistory);

            $em->flush();

            $this->addFlash('success', 'Merci d\'avoir proposé votre aide, '.$serviceRequest->getBeneficiary().' va bientôt rentrer en contact avec vous.');

            return $this->redirectToRoute('services_list');
            
        }else{
                    return $this->render('/requestDetails.html.twig', 
                    [ 'serviceRequest' => $serviceRequest, 
                    'form' => $form->createView()
                ]);
        } 
        
    } 

    /**
     * @Route ("/missions-en-cours/{id}", name="mission_details")
     */
    public function missionDetails($id, MissionRepository $mr, Request $request, EntityManagerInterface $em) : Response {

        $ongoingMission =  $mr
        ->findOneBy(
            ['id' => $id]
        );

        return $this->render('/missionDetails.html.twig', 
        [ 'mission' => $ongoingMission, ]);

    }
}
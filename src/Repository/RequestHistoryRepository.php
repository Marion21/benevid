<?php

namespace App\Repository;

use App\Entity\RequestHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequestHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestHistory[]    findAll()
 * @method RequestHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestHistory::class);
    }

    // /**
    //  * @return RequestHistory[] Returns an array of RequestHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RequestHistory
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

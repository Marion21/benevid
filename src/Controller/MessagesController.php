<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use App\Form\MessageResponseType;
use App\Repository\MessageRepository;
use App\Repository\MissionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessagesController extends AbstractController
{
    /**
     * @Route("/messages", name="messages")
     */
    public function index()
    {
        return $this->render('messages/index.html.twig', [
            'controller_name' => 'MessagesController',
        ]);
    }

    /**
     * @Route("/envoyer", name="send")
     */
    public function send(Request $request, EntityManagerInterface $manager, MissionRepository $missionRepository) : Response{

        $message = new Message;

        $user_id = $this->getUser()->getId();

        $matchingMissions = $missionRepository
        ->findContacts($user_id);

        $contacts = [];

        foreach($matchingMissions as $mission){

            $contacts[]=$mission->getBeneficiary();
        }
        
        $form = $this->createForm(MessageType::class, $message, array('contacts' => $contacts));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $message->setSender($this->getUser());
            $manager->persist($message);
            $manager->flush();

            $this->addFlash("message", "Votre message a bien été envoyé.");
            return $this->redirectToRoute("messages");
        }

        return $this->render("messages/send.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/lire/{id}", name="read")
     */
    public function read(EntityManagerInterface $manager, MessageRepository $messageRepository, $id) : Response{

        $message = $messageRepository
        ->findOneBy(
            ['id' => $id]
        );

        $message->setIsRead(true);
        $manager->persist($message);
        $manager->flush();

        return $this->render('messages/messageDetails.html.twig', [
            "message" => $message
        ]);
    }

     /**
     * @Route("/supprimer/{id}", name="delete")
     */
    public function delete(EntityManagerInterface $manager,MessageRepository $messageRepository, $id) : Response{

        $message = $messageRepository
        ->findOneBy(
            ['id' => $id]
        );

        $manager->remove($message);
        $manager->flush();

        return $this->redirectToRoute("messages");
    }

    /**
     * @Route("/répondre/{id}", name="response")
     */
    public function reply(MessageRepository $mr, EntityManagerInterface $em, Request $request, $id){

        $user = $this->getUser();

        $repliedMessage = $mr
        ->findOneBy(
            ["id" => $id]
        );

        $message = new Message();

        $recipient = $repliedMessage->getSender();
        $recipient_pseudo = $repliedMessage->getSender()->getPseudo();

        $form = $this->createForm(MessageResponseType::class, $message);
    
        $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        
        $message->setRecipient($recipient);
        $message->setCreatedAt(new \DateTime());
        $message->setIsRead(0);
        $message->setSender($user);

        $em->persist($message);
        $em->flush();

        $this->addFlash('success', 'Votre réponse a été envoyée.');
        
            return $this->redirectToRoute('messages');
    } 
        return $this->render('messages/replyTo.html.twig',
            ['form'=> $form->createView(),
            'recipient' => $recipient_pseudo
        ]);
    }


}

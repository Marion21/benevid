<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\MissionHistory;
use App\Entity\RequestHistory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ServiceRequestRepository;
use App\Repository\RequestHistoryRepository;
use App\Repository\MissionRepository;
use App\Repository\MissionHistoryRepository;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UserHistoryController extends AbstractController{

    //AFFICHER DEMANDES EN COURS
    /**
     * @Route ("/mon-profil/demandes-en-cours", name="ongoing_requests")
     */
    public function getOngoingRequests (ServiceRequestRepository $requestRepository) {

        $user_id = $this->getUser()->getId();

        $ongoingRequests = $requestRepository
        ->findBy(
            ['status' => ['CREATED','MISSION'],
            'beneficiary' => $user_id]
        );

        return $this->render('userProfileViews/ongoingRequests.html.twig', [
            'ongoingRequests' => $ongoingRequests
        ]);
    }

    //EDITER DEMANDES EN COURS
    /**
     * @Route("/mon-profil/demandes-en-cours/edition/{id}", name="request_update")
     */

     public function editRequest (ServiceRequestRepository $requestRepository, Request $request, EntityManagerInterface $em, $id){

        $user_id = $this->getUser()->getId();

        $ongoingRequests = $requestRepository
        ->findBy(
            ['status' => ['CREATED','MISSION'],
            'beneficiary' => $user_id]
        );

        $serviceRequest = $requestRepository
        ->findOneBy(
            ['id' => $id]
        );
        
        $form = $this->createFormBuilder($serviceRequest)
            ->add("title", TextType::class, [
                "attr" => [
                    "class" => "form-control",
                ], 
                "label" => "Titre"
            ])

            ->add("category", EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                "attr" => [
                    "class" => "form-control"
                ], 
                "label" => "Catégorie"
            ])

            ->add("description", TextareaType::class, [
                "attr" => [
                    "class" => "form-control"
                ], 
                "label" => "Description"
            ])

            ->add("servicePlace", TextareaType::class, [
                "attr" => [
                    "class" => "form-control"
                ], 
                "label" => "Endroit où le volontaire devra se rendre"
            ])

            ->add("submit", SubmitType::class,[
                "attr" => [
                    "class" => "btn btn-primary"
                ], 
                "label" => "Mettre à jour"
            ])
            ->getForm();

            $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $serviceRequest->setUpdatedAt(new \DateTime());
            $em->persist($serviceRequest);

            $associatedRequestHistory = new RequestHistory();
            $associatedRequestHistory->setServiceRequest($serviceRequest);
            $associatedRequestHistory->setStatus('EDITION');
            $associatedRequestHistory->setCreatedAt(new \DateTime());
            $em->persist($associatedRequestHistory);
        
            $em->flush();

            $this->addFlash('success', 'Votre demande a bien été mise à jour');

            return $this->redirect($this->generateUrl("ongoing_requests"));

        }else{

            return $this->render('userProfileViews/ongoingRequests.html.twig', [
                'ongoingRequests' => $ongoingRequests,
                'form' => $form->createView()
            ]);
        }
    }
    
    //ANNULER DEMANDES EN COURS
    /**
     * @Route("/mon-profil/demandes-en-cours/annulation/{id}", name="request_cancellation")
     */
    public function cancelRequest(ServiceRequestRepository $requestRepository, EntityManagerInterface $em, $id){

        $ongoingRequest = $requestRepository
        ->findOneBy(
            ['id' => $id]
        );
        $ongoingRequest->setStatus('CANCELLED');
        $ongoingRequest->setUpdatedAt(new \DateTime());
        $em->persist($ongoingRequest);

        $associatedRequestHistory = new RequestHistory();
        $associatedRequestHistory->setServiceRequest($ongoingRequest);
        $associatedRequestHistory->setStatus('CANCELLATION');
        $associatedRequestHistory->setCreatedAt(new \DateTime());
        $em->persist($associatedRequestHistory);

        $em->flush();

        $this->addFlash('success', 'Votre demande a bien été annulée.');

        return $this->redirect($this->generateUrl("ongoing_requests"));
    }

    // AFFICHER DEMANDES PASSEES
    /**
     * @Route ("/mon-profil/demandes-passées", name="past_requests")
     */
    public function getPastRequests (ServiceRequestRepository $requestRepository) {

        $user_id = $this->getUser()->getId();

        $pastRequests = $requestRepository
        ->findBy(
            ['status' => ['ARCHIVED', 'CANCELLED'],
            'beneficiary' => $user_id]
        );

        return $this->render('userProfileViews/pastRequests.html.twig', [
            'pastRequests' => $pastRequests
        ]);
    }

    //AFFICHER MISSIONS EN COURS
    /**
     * @Route ("/mon-profil/missions-en-cours", name="ongoing_missions")
     */
    public function getOngoingMissions (MissionRepository $missionRepository){

        $user_id = $this->getUser()->getId();

        $ongoingMissions = $missionRepository
        ->findBy(
            ['status' => 'ONGOING',
            'volunteer' => $user_id]
        );

        return $this->render('userProfileViews/ongoingMissions.html.twig', [
            'ongoingMissions' => $ongoingMissions
        ]);
    }

    //ANNULER MISSION EN COURS
    /**
     * @Route ("/mon-profil/missions-en-cours/annulation/{id}", name="mission_cancellation")
     */
    public function cancelMission(MissionRepository $missionRepository, ServiceRequestRepository $requestRepository, EntityManagerInterface $em, $id){

        $ongoingMission = $missionRepository
        ->findOneBy(
            ['id' => $id]
        );

        $serviceRequest = $ongoingMission->getServiceRequest();
        
        $ongoingMission->setStatus('CANCELLED');
        $ongoingMission->setUpdatedAt(new \DateTime());
        $em->persist($ongoingMission);

        $associatedMissionHistory = new MissionHistory();
        $associatedMissionHistory->setMission($ongoingMission);
        $associatedMissionHistory->setStatus('CANCELLATION');
        $associatedMissionHistory->setCreatedAt(new \DateTime());
        $em->persist($associatedMissionHistory);

        $serviceRequest->setStatus('CREATED');
        $serviceRequest->setUpdatedAt(new \DateTime());
        $em->persist($serviceRequest);

        $associatedRequestHistory = new RequestHistory();
        $associatedRequestHistory->setServiceRequest($serviceRequest);
        $associatedRequestHistory->setStatus('MISSION CANCELLATION');
        $associatedRequestHistory->setCreatedAt(new \DateTime());
        $em->persist($associatedRequestHistory);

        $em->flush();

        $this->addFlash('success', 'La mission a bien été annulée.');

        return $this->redirect($this->generateUrl("ongoing_missions"));
    }

    //MARQUER UNE DEMANDE DE SERVICE COMME TERMINEE - MISSION COMPLETEE
    /**
     * @Route("/mon-profil/demandes-en-cours/completion/{id}", name="request_completion")
     */
    public function confirmCompletion(ServiceRequestRepository $requestRepository, MissionRepository $missionRepository, EntityManagerInterface $em, $id){
        
        $serviceRequest = $requestRepository
        ->findOneBy(
            ['id' => $id]
        );

        $ongoingMission = $missionRepository
        ->findOneBy(
            ['serviceRequest' => $serviceRequest]
        );

        $serviceRequest->setStatus('ARCHIVED');
        $serviceRequest->setUpdatedAt(new \DateTime());
        $em->persist($serviceRequest);

        $associatedRequestHistory = new RequestHistory();
        $associatedRequestHistory->setServiceRequest($serviceRequest);
        $associatedRequestHistory->setStatus('ARCHIVED');
        $associatedRequestHistory->setCreatedAt(new \DateTime());
        $em->persist($associatedRequestHistory);

        $ongoingMission->setStatus('COMPLETED');
        $ongoingMission->setUpdatedAt(new \DateTime());
        $em->persist($ongoingMission);

        $associatedMissionHistory = new MissionHistory();
        $associatedMissionHistory->setMission($ongoingMission);
        $associatedMissionHistory->setStatus('COMPLETION');
        $associatedMissionHistory->setCreatedAt(new \DateTime());
        $em->persist($associatedMissionHistory);

        $em->flush();
 
        $this->addFlash('success', 'Votre demande a bien été marquée comme terminée.');

        return $this->redirect($this->generateUrl("ongoing_requests"));
    }


    //AFFICHER MISSIONS PASSEES
    /**
     * @Route("/mon-profil/missions-passees", name="past_missions")
     */
    public function getPastMissions(MissionRepository $missionRepository){

        $user_id = $this->getUser()->getId();

        $pastMissions = $missionRepository
        ->findBy(
            ['status' => ['COMPLETED','CANCELLED'],
            'volunteer' => $user_id]
        );

        return $this->render('userProfileViews/pastMissions.html.twig', [
            'pastMissions' => $pastMissions
        ]);
    }
}
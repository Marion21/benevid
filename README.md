Réalisation du projet "Benevid" imaginé pendant le concours City'ZEN Challenge organisé par Berger-Levrault

Site web jouant le rôle d'une plateforme permettant à ses utilisateurs de poster des demandes de services (ex : courses, retrait colis, etc) et/ou de se porter volontaire pour répondre aux besoins des autres utilisateurs.

Projet réalisé avec le framework Symfony

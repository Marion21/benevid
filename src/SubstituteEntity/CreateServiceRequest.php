<?php

namespace App\Domain;

use App\Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;


class CreateServiceRequest
{

    /**
     * @Assert\NotNull(message="Veuillez décrire votre besoin dans ce champ")
     * @Assert\Length(max="255")
     * @Assert\NotBlank(message="")
     * @var string
     */
    private $title;

    private $category;

    /**
     * @Assert\NotNull(message="Veuillez décrire votre besoin dans ce champ")
     * @Assert\NotBlank(message="")
     * @var string
     */
    private $description;

    /**
    * @Assert\Length(max="255")
    */
    private $servicePlace;

    /**
     * @Assert\Length(max="255")
     * @var string
     */
    private $address;

    /**
     * @Assert\Length(max="255")
     * @var string
     */
    private $zipCode;

       /**
     * @Assert\Length(max="255")
     * @var string
     */
    private $city;




    /**
     * Get the value of title
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  string  $title
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of category
     */ 
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of category
     *
     * @return  self
     */ 
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @return  string
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string  $description
     *
     * @return  self
     */ 
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of servicePlace
     */ 
    public function getServicePlace()
    {
        return $this->servicePlace;
    }

    /**
     * Set the value of servicePlace
     *
     * @return  self
     */ 
    public function setServicePlace($servicePlace)
    {
        $this->servicePlace = $servicePlace;

        return $this;
    }

    /**
     * Get the value of address
     *
     * @return  string
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @param  string  $address
     *
     * @return  self
     */ 
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of zipCode
     *
     * @return  string
     */ 
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set the value of zipCode
     *
     * @param  string  $zipCode
     *
     * @return  self
     */ 
    public function setZipCode(string $zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get the value of city
     *
     * @return  string
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @param  string  $city
     *
     * @return  self
     */ 
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }
}

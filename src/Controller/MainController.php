<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ContactType;
use App\Repository\ServiceRequestRepository;
use App\Repository\MissionRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MainController extends AbstractController{

/**
     * @Route("/", name="home")
     */
    public function home(ServiceRequestRepository $requestRepository, MissionRepository $missionRepository): Response
    {
        $servicesList = $requestRepository
        ->findBy(
            ['status' => 'CREATED']
        );

        $completedMissions = $missionRepository
        ->findBy(
            ['status' => 'COMPLETED',
        ]);
        $nbcompletedMissions = count($completedMissions);


        return $this->render('homepage.html.twig', [
            'controller_name' => 'HomeController',
            'servicesList' => $servicesList,
            'nbCompletedMissions' => $nbcompletedMissions
        ]);
    }

 /**
     * @Route("/faq", name="faq")
     */
    public function faq(){

        return $this->render('faq.html.twig');
    }

    /**
     * @Route("/a-propos")
     */
    public function about(){

        return $this->render('about.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, MailerInterface $mailer){

        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){

            $senderEmail = $form->getData()["email"];
            $messageTitle = $form->getData()["title"];
            $messageContent = $form->getData()["message"];

            $email = (new TemplatedEmail())
            ->from($senderEmail)
            ->to('marion.coudrais@live.fr')
            ->subject($messageTitle)
            ->text($messageContent)
            ->html('<p>'.$messageContent.'</p>');

            $mailer->send($email);

            $this->addFlash('success', 'Votre message a bien été envoyé, vous recevrez une réponse prochainement.');

            return $this->redirectToRoute('contact');
        }else{
            return $this->render('/contact.html.twig', [
                'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/mentions-legales", name="legal_mentions")
     */
    public function legalMentions(){

        return $this->render('/legalMentions.html.twig');
    }

    /**
     * @Route("/plan-du-site", name="sitemap", defaults={"_format"="xml"})
     */
    public function sitemap(Request $request){

        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('inscription')];
        $urls[] = ['loc' => $this->generateUrl('faq')];
        $urls[] = ['loc' => $this->generateUrl('contact')];
        $urls[] = ['loc' => $this->generateUrl('legal_mentions')];
        $urls[] = ['loc' => $this->generateUrl('request_posting')];
        $urls[] = ['loc' => $this->generateUrl('messages')];
        $urls[] = ['loc' => $this->generateUrl('ongoing_requests')];
        $urls[] = ['loc' => $this->generateUrl('ongoing_missions')];
        $urls[] = ['loc' => $this->generateUrl('past_missions')];
        $urls[] = ['loc' => $this->generateUrl('user_profile')];
        $urls[] = ['loc' => $this->generateUrl('user_modifications')];

        // Fabrication de la réponse XML
        $response = new Response(
            $this->renderView('sitemap.html.twig', 
                ['urls' => $urls,
                'hostname' => $hostname]),
            200
        );

        // Ajout des entêtes
        $response->headers->set('Content-Type', 'text/xml');

        // On envoie la réponse
        return $response;        
    }
}
<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210726093642 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission ADD service_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23CD42F8111 FOREIGN KEY (service_request_id) REFERENCES service_request (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9067F23CD42F8111 ON mission (service_request_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23CD42F8111');
        $this->addSql('DROP INDEX UNIQ_9067F23CD42F8111 ON mission');
        $this->addSql('ALTER TABLE mission DROP service_request_id');
    }
}

<?php

namespace App\Repository;

use App\Entity\MissionHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MissionHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissionHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissionHistory[]    findAll()
 * @method MissionHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissionHistory::class);
    }

    // /**
    //  * @return MissionHistory[] Returns an array of MissionHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MissionHistory
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

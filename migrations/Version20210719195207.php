<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210719195207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission (id INT AUTO_INCREMENT NOT NULL, volunteer_id INT NOT NULL, beneficiary_id INT NOT NULL, status VARCHAR(16) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9067F23C8EFAB6B1 (volunteer_id), INDEX IDX_9067F23CECCAAFA0 (beneficiary_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission_history (id INT AUTO_INCREMENT NOT NULL, mission_id INT NOT NULL, status VARCHAR(32) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B686E406BE6CAE90 (mission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request_history (id INT AUTO_INCREMENT NOT NULL, service_request_id INT NOT NULL, status VARCHAR(32) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_D9E021CD42F8111 (service_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service_request (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, beneficiary_id INT NOT NULL, title VARCHAR(225) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_F413DD0312469DE2 (category_id), INDEX IDX_F413DD03ECCAAFA0 (beneficiary_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, lastname VARCHAR(32) NOT NULL, email VARCHAR(128) NOT NULL, address VARCHAR(255) NOT NULL, zip_code VARCHAR(8) NOT NULL, city VARCHAR(64) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, status VARCHAR(32) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23C8EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23CECCAAFA0 FOREIGN KEY (beneficiary_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE mission_history ADD CONSTRAINT FK_B686E406BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id)');
        $this->addSql('ALTER TABLE request_history ADD CONSTRAINT FK_D9E021CD42F8111 FOREIGN KEY (service_request_id) REFERENCES service_request (id)');
        $this->addSql('ALTER TABLE service_request ADD CONSTRAINT FK_F413DD0312469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE service_request ADD CONSTRAINT FK_F413DD03ECCAAFA0 FOREIGN KEY (beneficiary_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_request DROP FOREIGN KEY FK_F413DD0312469DE2');
        $this->addSql('ALTER TABLE mission_history DROP FOREIGN KEY FK_B686E406BE6CAE90');
        $this->addSql('ALTER TABLE request_history DROP FOREIGN KEY FK_D9E021CD42F8111');
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23C8EFAB6B1');
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23CECCAAFA0');
        $this->addSql('ALTER TABLE service_request DROP FOREIGN KEY FK_F413DD03ECCAAFA0');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE mission_history');
        $this->addSql('DROP TABLE request_history');
        $this->addSql('DROP TABLE service_request');
        $this->addSql('DROP TABLE user');
    }
}

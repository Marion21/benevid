<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\User;
use App\Entity\ServiceRequest;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ServiceRequestRepository;
use App\Repository\MissionRepository;
use App\Repository\UserRepository;
use App\Form\RegistrationType;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\ORM\EntityManagerInterface;


class UserProfileController extends AbstractController{

    /**
     * @Route ("/mon-profil", name="user_profile")
     */
    public function getUserHistory (ServiceRequestRepository $requestRepository, MissionRepository $missionRepository) {

        $user_id = $this->getUser()->getId();

        //Requêtes en cours, pas encore pourvues
        $ongoingRequests = $requestRepository
        ->findBy(
            ['status' => ['CREATED','MISSION'],
            'beneficiary' => $user_id]
        );
        $nbOngoingRequests = count($ongoingRequests);


        //Requêtes dont les missions sont complétées ou qui ont été annulées
        $pastRequests = $requestRepository
        ->findBy(
            ['status' => ['ARCHIVED', 'CANCELLED'],
            'beneficiary' => $user_id]
        );
        $nbPastRequests = count($pastRequests);


        //Missions en cours
        $ongoingMissions = $missionRepository
        -> findBy(
            ['status' => 'ONGOING',
            'volunteer' => $user_id]
        );
        $nbOngoingMissions = count($ongoingMissions);


        //Missions annulées ou terminées
        $pastMissions = $missionRepository
        ->findBy(
            ['status' => ['COMPLETED', 'CANCELLED'], 
            'volunteer' => $user_id]
        );
        $nbPastMissions = count($pastMissions);

        return $this->render('userProfileViews/userProfile.html.twig', [
            'nbOngoingRequests' => $nbOngoingRequests,
            'nbPastRequests' => $nbPastRequests,
            'nbOngoingMissions' => $nbOngoingMissions,
            'nbPastMissions' => $nbPastMissions
        ]);

    }
    /**
     * @Route("/mon-profil/modifier-mes-informations", name="user_modifications")
     */
    public function changeUserInformations(UserRepository $userRepository, EntityManagerInterface $em, Request $request,  UserPasswordEncoderInterface $encoder){

        $current_user_id= $this->getUser()->getId();

        $user = $userRepository
        ->findOneBy(
            ['id' => $current_user_id]
        ); 

      /*   $form = $this->createFormBuilder($user)
        ->add("pseudo", TextType::class, [
            "attr" => [
                "class" => "form-control",
            ], 
            "label" => "Prénom"
        ])

        ->add("email", EmailType::class, [
            "attr" => [
                "class" => "form-control"
            ], 
            "label" => "Adresse email"
        ])

        ->add("address", TextType::class, [
            "attr" => [
                "class" => "form-control"
            ], 
            "label" => "Adresse"
        ])

        ->add("zipCode", TextType::class, [
            "attr" => [
                "class" => "form-control"
            ], 
            "label" => "Code postal"
        ])

        ->add("city", TextType::class, [
            "attr" => [
                "class" => "form-control"
            ], 
            "label" => "Ville"
        ])

        ->add("submit", SubmitType::class,[
            "attr" => [
                "class" => "btn btn-success"
            ], 
            "label" => "Mettre à jour"
        ])
        ->getForm(); */

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
           /*  $user->setUpdatedAt(new \DateTime());
            $em->persist($user);
            $em->flush(); */

            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $user->setUpdatedAt(new \DateTime());
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Vos informations ont bien été mises à jour');
            
                return $this->redirectToRoute('user_profile');
        } 

        return $this->render('userProfileViews/changeUserInfo.html.twig',
            ['form'=> $form->createView()
        ]);

    }
        
 


   
}